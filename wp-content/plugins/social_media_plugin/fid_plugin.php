<?php
/**
 * Plugin Name: Social Media Plugin
 * Description: Extension pour les Réseaux Sociaux 
 * Author: Malo BERGADE, Ivo ANTUNOVIC, Benjamin LEBON 
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__)."/services/Fid_Database_Service.php";
require_once plugin_dir_path(__FILE__)."Fid_List.php";
class Fid
{
    public function __construct()
    {
        register_activation_hook( __FILE__, array("Fid_Database_Service", "create_db") );
        add_action('admin_menu', array($this, 'add_menu_guests'));

    }

    public function add_menu_guests()
    {
        add_menu_page("Les invités de l'évènement",
            "Invités", "manage_options",
            "fidGuest",
            array($this, "my_guests"),
            "dashicons-groups",
            40 );

        add_submenu_page("fidGuest",
            "Ajouter un invité", "Ajouter",
            "manage_options", 'addGuest',
            array($this, "my_guests"));
    }

    public function my_guests()
    {
        echo "<h1>". get_admin_page_title() ."</h1>";
        $db = new Fid_Database_Service();

        if( $_REQUEST['page'] == 'fidGuest' ||
            ( $_POST['firstname'] && $_POST['lastname'] && $_POST['description'] && $_POST['image_url']) ) {
        

//             Suppression via l'interface d'action directe
            if( $_REQUEST['page'] == 'fidGuest' && $_GET['id'])
                $db->deleteGuest($_GET['id']);

            if($_POST['firstname'] && $_POST['lastname'] && $_POST['description'] && $_POST['image_url'] )
                $db->saveGuest(); // sauvegarde avec les champs de formulaire

            echo "<form method='post'>";
            $table = new Fid_List();
            $table->prepare_items();
            echo $table->display();

    } else {
        echo '
                <form method="POST" enctype="multipart/form-data">
        
                    <label for="nom">nom</label>
                    <input class="widefat" type="text" name="lastname" required>
                    
                    <label for="prenom">prenom</label>
                    <input class="widefat" type="text" name="firstname" required>
                    
                    <label for="description">description</label>
                    <input class="widefat" type="text" name="description" required>
                    
                    <label for="image_url">image_url</label>
                    <input class="widefat" type="file" name="image_url" required>
            
                    <label for="">Facebook</label>
                    <input class="widefat" type="text" name="facebook" >
                    
                    <label for="">Twitter</label>
                    <input class="widefat" type="text" name="twitter" >
            
                    <label for="">Website</label>
                    <input class="widefat" type="text" name="website" >
            
                    <label for="">Youtube</label>
                    <input class="widefat" type="text" name="youtube" > 
            
                    <label for="">Flickr</label>
                    <input class="widefat" type="text" name="flickr" >
                    
                    <label for="">Instagram</label>
                    <input class="widefat" type="text" name="instagram" >
                    
                    <input type="submit">
                </form>';

            if ($_POST['lastname'] && $_POST['firstname'] && $_POST['description'] && ['image_url'] )
                $db->saveGuest(); // sauvegarde avec les champs de formulaire
        }
    
    }
}

new Fid();