<?php

class Fid_Database_Service
{
    public function __construct()
    {
    }


    public function findAll()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}guests");
        return $res;
    }

    public static function create_db() {
        global $wpdb; // Connexion a la BDD
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            " {$wpdb->prefix}guests ( ".
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " nom VARCHAR(50) NOT NULL, ".
            " prenom VARCHAR(50) NOT NULL, ".
            " description VARCHAR(100) NOT NULL, ".
            " image_url VARCHAR(100) NOT NULL);"
        );

        $wpdb->query("CREATE TABLE IF NOT EXISTS " .
            " {$wpdb->prefix}networks ( ".
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " label VARCHAR(45) NOT NULL); "
        );


        $wpdb->query("CREATE TABLE IF NOT EXISTS " .
            " {$wpdb->prefix}urls ( ".
            " id INT unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY, ".
            " guests_id INT unsigned DEFAULT NULL, ".
            " network_id INT unsigned DEFAULT NULL, ".
            " url_network VARCHAR(100) NOT NULL); "
//            " INDEX FK_Urls_Guests_idx (guests_id), ".
//            " INDEX FK_Networks_Guests_idx (network_id), ".
//            " FOREIGN KEY (guests_id) REFERENCES {$wpdb->prefix}guests (id), ".
//
//            " FOREIGN KEY (network_id) REFERENCES {$wpdb->prefix}networks (id)); "


        );



    }
    /**
     * Méthode de Sauvegarde du formulaire
     */
    public function saveGuest()
    {
        global $wpdb;

        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $description = $_POST['description'];
        $arr = array(
            1 => $_POST['facebook'],
            2 => $_POST['twitter'],
            3 => $_POST['website'],
            4 => $_POST['youtube'],
            5 => $_POST['flickr'],
            6 => $_POST['instagram']
        );

        if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $uploadedfile = $_FILES['image_url'];
        $upload_overrides = array( 'test_form' => false );
        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
        if ( $movefile ) {
            $wp_filetype = $movefile['type'];
            $filename = $movefile['file'];
            $wp_upload_dir = wp_upload_dir();
            $attachment = array(
                'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
                'post_mime_type' => $wp_filetype,
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            $attach_id = wp_insert_attachment( $attachment, $filename, $insertPost);
            $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
            wp_update_attachment_metadata( $attach_id, $attach_data );
        //file is uploaded successfully. do next steps here.
        }
    

      

        $wpdb->insert("{$wpdb->prefix}guests", array(
            'nom' => strtoupper($lastname),
            'prenom' => ucfirst(strtolower($firstname)),
            'description' => $description,
            'image_name' => preg_replace('/app\//','',$filename)
        ));


        $last_id = $wpdb->insert_id;

        foreach($arr as $key => $url)
        {
            $wpdb->insert("{$wpdb->prefix}urls", array(
                'guests_id' => $last_id,
                'network_id' => $key,
                'url_network' =>  $url
            ));
        }

    }

    /**
     * Méthode de suppression d'un invité
     */
    public function deleteGuest($ids)
    {
        global $wpdb;

        if( ! is_array($ids) ){
            $ids = array($ids);
        }

        // $wpdb->query("DELETE from {$wpdb->prefix}guests WHERE id = {$ids}");
        $wpdb->query("delete from {$wpdb->prefix}guests where id in (" .
            implode(',', $ids) . ");");
            
        $wpdb->query("delete from {$wpdb->prefix}urls where guests_id in(" .
        implode(',', $ids) . ");");

    
    }
}
