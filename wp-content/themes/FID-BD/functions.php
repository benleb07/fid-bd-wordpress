<?php


function ern_setup()
{
    global $content_width;
    if( ! isset($content_width)){
        $content_width = 2500;
    }
    /**
     * permet les liens rss sur les posts et comment
     */
    add_theme_support('automatic-feed-links');

    add_theme_support('post-thumbnails');

    $args = array(
        'default-text-color' => '000',
        'width' => 1000,
        'height' => 2500,
        'flex-width' => true,
        'flex-height' => true
    );

    add_theme_support('custom-header', $args);
}

add_action('after_setup_theme', 'ern_setup');

function register_menu()
{
    register_nav_menus(
        array(
            "menu-sup" => __("Menu sup"),
            "menu-res" => __("Menu sociaux")
        )
    );
}
add_action('init', 'register_menu');
class Ern_walker extends Walker_Nav_Menu
{
    public function start_el(&$output, $item, $depth = 0, $args = null, $id = 0)
    {
        $title = $item->title;
        $description = $item->description;
        $permalien = $item->url;

        if($title == 'Accueil')
        {
            $output.="<li class='menu-active'>";
        }
        else
        {
            $output .= "<li>";
        }


        if( $permalien ) 
        {
            $output .= "<a href='" . $permalien ."'>";
        } 
        else
        {
            $output .= "<span>";
        }

        $output .= $title;

        if( $description != "" && $depth == 0)
        {
            $output .= "<small class='description'>" . $description . "</small>";
        }
        if( $permalien  && $permalien !="#") 
        { // si on à un lien dans notre item
            $output .= "</a>";
        }
        else 
        {
            $output .= "</span>";
        }
    }
    
}

function theme_script()
{
    // JS du site
    wp_register_script(
        'monjs',
        get_template_directory_uri() . '/js/main.js',
        array('jquery'),
        true,
        true
    );
    wp_enqueue_script('monjs');
    // Google Fonts
    wp_register_style(
        'gfont',
        'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800',
        array(),
        null,
        'all'
    );
    wp_enqueue_style('gfont');
    // Bootstrap
    // CSS
    wp_register_style(
        'boot',
        get_template_directory_uri() . "/css/bootstrap.css",
        array(),
        true
    );
    wp_enqueue_style('boot');
    // JS
    wp_register_script(
        'boot_js',
        get_template_directory_uri() . "/js/bootstrap.bundle.min.js",
        array(),
        true
    );
    wp_enqueue_script('boot_js');
    
    // Animate CSS
    wp_register_style(
        'animate_js',
        get_template_directory_uri() . "/css/animate.min.css",
        array(),
        true
    );
    wp_enqueue_style('animate_js');
    // Easing JS
    wp_register_script(
        'easing_js',
        get_template_directory_uri() . "/js/easing.min.js",
        array(),
        true
    );
    wp_enqueue_script('easing_js');
    // Owl carousel
    // min
    wp_register_script(
        'owlcar_js',
        get_template_directory_uri() . "/js/owl.carousel.min.js",
        array(),
        true
    );
    wp_enqueue_script('owlcar_js');
    // CSS
    wp_register_style(
        'owlcar_css',
        get_template_directory_uri() . "/css/owl.carousel.min.css",
        array(),
        true
    );
    wp_enqueue_style('owlcar_css');
    // Superfish
    // min
    wp_register_script(
        'superfish_js',
        get_template_directory_uri() . "/js/superfish.min.js",
        array(),
        true
    );
    wp_enqueue_script('superfish_js');
    // hover
    wp_register_style(
        'superfish_hover_js',
        get_template_directory_uri() . "/js/hoverIntent.js",
        array(),
        true
    );
    wp_enqueue_script('superfish_hover_js');
    // Venobox
    // CSS
    wp_register_style(
        'venobox_css',
        get_template_directory_uri() . "/css/venobox.css",
        array(),
        true
    );
    wp_enqueue_style('venobox_css');
    // JS
    wp_register_script(
        'venobox_js',
        get_template_directory_uri() . "/js/venobox.min.js",
        array(),
        true
    );
    wp_enqueue_script('venobox_js');
    // wow
    wp_register_script(
        'wow_js',
        get_template_directory_uri() . "/js/wow.min.js",
        array('jquery'),
        true
    );
    wp_enqueue_script('wow_js');
    // Font Awesome
    wp_register_style(
        'font_awesome',
        get_template_directory_uri() . "/font-awesome/css/font-awesome.min.css",
        array(),
        true
    );
    wp_enqueue_style('font_awesome');
    // CSS du site
    wp_register_style(
        'main_style',
        get_template_directory_uri() . "/css/main_style.css",
        array(),
        true
    );
    wp_enqueue_style('main_style');
}
add_action('wp_enqueue_scripts','theme_script');
