
<?php get_header(); ?>
    <section id="intro" style="background: url(<?php echo header_image()?>)">
        <div class="intro-container wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
            <h1 class="mb-4 pb-0"><span>F</span>ESTIVAL <span>I</span>NTERNATIONAL<br>DU<span> D</span>ISQUE &amp; DE LA <span>B</span>ANDE <span>D</span>ESSINÉE</h1>

           


           <?php 

           $date_event_query = new WP_Query('category_name=date-et-lieu-du-festival');
           /** BOUCLE **/
                 while($date_event_query->have_posts()) :  
                 $date_event_query->the_post();
            ?>
                       
            <p class="mb-4 pb-0"><?php the_content(); ?></p>

           <?php endwhile; ?>


         <?php 
           $video_query = new WP_Query('category_name=video-presentation-festival');
           /** BOUCLE **/
                 while($video_query->have_posts()) :  
                 $video_query->the_post();

            ?>

            

            <a href="<?php echo get_post_meta(get_the_ID(), 'video-presentation-festival', true); ?>" class="venobox play-btn mb-4 vbox-item" data-vbtype="video" data-autoplay="true"></a>


            <?php endwhile; ?>



            <a href="/#about" class="about-btn scrollto">À propos du Festival</a>
        </div>
    </section>

    <main id="main">

        <!-- Section A PROPOS -->
        <section id="about">
            <div class="container">
                <div class="row">

                 <?php 
                    $about_event_query = new WP_Query('category_name=a-propos-du-festival');
                    /** BOUCLE **/
                    while($about_event_query->have_posts()) :
                          $about_event_query->the_post();                
                 ?>

                    <div class="col-lg-6">
                        <h2>À propos du festival FID&amp;BD</h2>
                        <p><?php the_content(); ?></p>
                    </div>

                    <div class="col-lg-3">
                        <h3>Où ?</h3>
                        <p><?php echo get_post_meta(get_the_ID(), 'location-event', true); ?></p>
                    </div>
                    <div class="col-lg-3">
                        <h3>Quand ?</h3>
                        <p> <?php echo  get_post_meta(get_the_ID(), 'days-event', true); ?> <br><?php echo get_post_meta(get_the_ID(), 'date-event', true); ?></p>
                    </div>

                <?php endwhile; ?>

                </div>
            </div>
        </section>

        <!-- Section invités -->
        <section id="guests" class="wow fadeInUp" style="visibility: hidden; animation-name: none;">
            <div class="container">
                <div class="section-header">
                    <h2>Les Invités</h2>
                    <p>Ils vous attendent pour vos dédicaces</p>
                </div>

                <div class="row">
                
                <?php 
                    global $wpdb; 
                    $bob = $wpdb->get_results(
                        "SELECT * FROM {$wpdb->prefix}guests"
                    );
                    function getNetworkclass($labelReseaux)
                    {
                        switch($labelReseaux)
                        {
                            
                            case 'facebook':
                                return 'fa fa-facebook';
                                break;

                            case 'twitter':
                                return 'fa fa-twitter';
                                break;

                            case 'website':
                                return 'fa fa-globe';
                                break;
                                
                            case 'youtube':
                                return 'fa fa-youtube';
                                break;

                            case 'flickr':
                                return 'fa fa-flickr';
                                break;

                            case 'instagram':
                                return 'fa fa-instagram';
                                break;

                            default:
                                return '';
                                break;
                                
                                
                        }
                    }
                    foreach($bob as $key=>$value):
                        global $wp;
                        $image_path = home_url($wp->request).$value->image_name;

                ?>




                        <div class="col-lg-4 col-md-6">
                            <div class="invit">
                                <img src="<?php echo $image_path?>" alt="<?php echo $value->prenom . ' '; echo $value->nom?>" class="img-fluid">
                                <div class="details">
                                    <h3><?php echo $value->nom. ' ' ;echo $value->prenom?></h3>
                                    <p> <?php echo $value->description ?> </p>

                                    <div class="social">
                                        <?php 
                                        $reseaux = $wpdb->get_results(
                                            "SELECT * FROM {$wpdb->prefix}urls 
                                                JOIN {$wpdb->prefix}networks on {$wpdb->prefix}urls.network_id = {$wpdb->prefix}networks.id
                                                WHERE {$wpdb->prefix}urls.guests_id = {$value->id}");

                                        foreach($reseaux as $key =>$reseau):
                                            if(!empty( $reseau->url_network)):
                                        ?>
                                        
                                        <a href="<?php echo $reseau->url_network?>" target="blank"><i class="<?php echo getNetworkclass($reseau->label)?>"></i></a>
                                        <?php endif;?> 
                                        <?php endforeach;?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>                    
                </div>
            </div>

        </section>

        <!-- Section programme -->
        <section id="prog" class="section-with-bg">
            <div class="container wow fadeInUp" style="visibility: hidden; animation-name: none;">
                <div class="section-header">
                    <h2>Le Programme</h2>
                    <p>Découvrez l'intégralité de nos évènements</p>
                </div>

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#ap" role="tab" data-toggle="tab">Avant-première</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#expo" role="tab" data-toggle="tab">Expositions BD</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#cs" role="tab" data-toggle="tab">Concerts / Soirées</a>
                    </li>
                </ul>

                <h3 class="sub-heading">Gratuité des expositions et des concerts / Entrée du FID : 3€</h3>

                <div class="tab-content row justify-content-center">
              
                <?php 
                    $preview_query = new WP_Query('category_name=avant-premiere');
                    /** BOUCLE **/
                    while($preview_query->have_posts()) :
                          $preview_query->the_post();


                 ?>

                    <!-- Programme avant-première -->
                    <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="ap">

                        <div class="row prog-item">
                            <div class="col-md-2"><time><?php echo get_post_meta(get_the_ID(), 'date-event', true);?></time></div>
                            <div class="col-md-10 <?php $value->decription ?> 10">
                                <h4><?php the_title(); ?></h4>
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div>

                    <?php endwhile; ?>

                    <?php 
                    $expo_query = new WP_Query('category_name=expositions-bd');
                    /** BOUCLE **/
                    while($expo_query->have_posts()) :
                          $expo_query->the_post();

                    ?>

                    <!-- Programme expositions -->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="expo">

                        <div class="row prog-item">
                            <div class="col-md-2"><time><?php echo get_post_meta(get_the_ID(), 'date-event', true); ?></time></div>
                            <div class="col-md-10">
                                <h4><?php the_title(); ?></h4>
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>

                    </div> 

                 <?php endwhile; ?>
      
                 <?php 
                    $concerts_evenings_query = new WP_Query('category_name=concerts-soirees');
                    /** BOUCLE **/
                    while($concerts_evenings_query->have_posts()) :
                          $concerts_evenings_query->the_post();

                    ?>

                    <!-- Programme concerts et soirées -->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="cs">

                        <div class="row prog-item">
                            <div class="col-md-2"><time><?php echo get_post_meta(get_the_ID(), 'date-event', true); ?></time></div>
                            <div class="col-md-10">
                                <h4><?php the_title(); ?></h4>
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div> 

                <?php endwhile; ?>

                </div>
            </div>

        </section>
s
        <!-- Section partenaires -->
        <section id="partners" class="section-with-bg wow fadeInUp" style="visibility: hidden; animation-name: none;">

            <div class="container">
                <div class="section-header">
                    <h2>Nos Partenaires</h2>
                </div>

                <div class="row no-gutters partners-wrap clearfix">



                <?php 
                    $partnair = new WP_Query('category_name=mes-partenaires ');
                    /** BOUCLE **/
                    while($partnair->have_posts()) :
                    $partnair->the_post();

                 ?>


                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="partner-logo">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid" alt="">
                        </div>
                    </div>

                    <?php endwhile; ?>

                </div>

            </div>

        </section>

        <!-- Contact Section -->
        <section id="contact" class="section-bg wow fadeInUp" style="visibility: hidden; animation-name: none;">

            <div class="container">

                <div class="section-header">
                    <h2>Pour nous trouver</h2>
                    <p>Église des Dominicains 66000 PERPIGNAN<br>Téléphone : 06 81 41 61 46</p>
                </div>

                <div class="row localisation">
                    <div class="col-lg-10 map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.148655122011!2d2.8974282148448713!3d42.70057302152288!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b06fad7c818c49%3A0xaa76d749aa613bf8!2sEglise%20des%20Dominicains!5e0!3m2!1sfr!2sfr!4v1598183642937!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>                    
                    </div>
                </div>

                <form method="post" action="<?php echo get_template_directory_uri()?>/mail/mail.php">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="name" placeholder="Nom complet" class="required">
                        </div>
                        <div class="col-md-12">
                            <input type="email" name="email" placeholder="E-mail" class="contact-form-email required">
                        </div>
                        <div class="col-md-12">
                            <input type="text" name="subject" placeholder="Sujet" class="contact-form-subject required">
                        </div>
                    </div>
                    <textarea name="message" placeholder="Votre message" class="required" rows="7"></textarea>
                    <div class="response-message"></div>
                    <button class="border-button border-bt-red" type="submit" id="submit" name="submit">Envoyer Message</button>
                </form>

            </div>
        </section>

    </main>

    <!-- Footer -->
    <footer id="footer">

        <div class="container">
            <div class="copyright">
                ©<strong> FID&amp;BD</strong> 2020. Tous droits réservés
            </div>
            <div class="credits">
                Design et développement <a href="https://www.graphicalizer.com/" target="blank">Yacine KERROUCHE</a>
            </div>
        </div>
    </footer>

    <!--  Bouton retour vers le haut -->
    <a href="http://festival-du-disque-bd.com/#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

<div class="">
    <h2></h2>
    <div class="">
        <div class=" ">
            <?php
            if( have_posts() ) : while( have_posts() ) : the_post();
                get_template_part('content', get_post_format());
                endwhile; endif;
            ?>
        </div>
    </div>
</div>
<?php get_template_part('partials/contact'); ?>

<?php get_footer();  ?>


/** IVO MALO BENJAMIN */