
<?php
    wp_head();
    ?>

<body>
    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left">
                <h1>
                    <a href="<?php echo get_bloginfo('wpurl') ?>">
                        <span>FID</span>&amp;<span>BD</span>
                    </a>
                </h1>
            </div>

            <?php wp_nav_menu( array(
                'theme_location'    => 'menu-sup',
                'container'         => 'nav',
                'container_id'      => 'nav-menu-container',
                'menu_class'        => 'nav-menu sf-js-enabled sf-arrows',
                'menu_id'           => '',
                'walker'            => new Ern_walker()));
            ?>
        </div>
    </header>