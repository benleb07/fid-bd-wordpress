-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ern2021
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2021-06-17 13:08:06','2021-06-17 13:08:06','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'post-trashed','','comment',0,0);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_guests`
--

DROP TABLE IF EXISTS `wp_guests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_guests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_guests`
--

LOCK TABLES `wp_guests` WRITE;
/*!40000 ALTER TABLE `wp_guests` DISABLE KEYS */;
INSERT INTO `wp_guests` VALUES (1,'MIKAïLOFF','Pierre','musicien, écrivain, journaliste et scénariste','/wp-content/uploads/2021/06/Mikailoff-3.jpg');
/*!40000 ALTER TABLE `wp_guests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_networks`
--

DROP TABLE IF EXISTS `wp_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_networks`
--

LOCK TABLES `wp_networks` WRITE;
/*!40000 ALTER TABLE `wp_networks` DISABLE KEYS */;
INSERT INTO `wp_networks` VALUES (1,'facebook'),(2,'twitter'),(3,'website'),(4,'youtube'),(5,'flickr'),(6,'instagram');
/*!40000 ALTER TABLE `wp_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://tp-wordpress.lndo.site','yes'),(2,'home','http://tp-wordpress.lndo.site','yes'),(3,'blogname','Test Blog Title','yes'),(4,'blogdescription','Just another WordPress site','yes'),(5,'users_can_register','0','yes'),(6,'admin_email','no-reply@extly.com','yes'),(7,'start_of_week','1','yes'),(8,'use_balanceTags','0','yes'),(9,'use_smilies','1','yes'),(10,'require_name_email','1','yes'),(11,'comments_notify','1','yes'),(12,'posts_per_rss','10','yes'),(13,'rss_use_excerpt','0','yes'),(14,'mailserver_url','mail.example.com','yes'),(15,'mailserver_login','login@example.com','yes'),(16,'mailserver_pass','password','yes'),(17,'mailserver_port','110','yes'),(18,'default_category','1','yes'),(19,'default_comment_status','open','yes'),(20,'default_ping_status','open','yes'),(21,'default_pingback_flag','1','yes'),(22,'posts_per_page','10','yes'),(23,'date_format','F j, Y','yes'),(24,'time_format','g:i a','yes'),(25,'links_updated_date_format','F j, Y g:i a','yes'),(26,'comment_moderation','0','yes'),(27,'moderation_notify','1','yes'),(28,'permalink_structure','/index.php/%year%/%monthnum%/%day%/%postname%/','yes'),(29,'rewrite_rules','a:96:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:57:\"index.php/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:45:\"index.php/category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:27:\"index.php/category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:54:\"index.php/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:49:\"index.php/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:30:\"index.php/tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:42:\"index.php/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:24:\"index.php/tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:55:\"index.php/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:50:\"index.php/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:31:\"index.php/type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:43:\"index.php/type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:25:\"index.php/type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:42:\"index.php/feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:37:\"index.php/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:18:\"index.php/embed/?$\";s:21:\"index.php?&embed=true\";s:30:\"index.php/page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:51:\"index.php/comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:46:\"index.php/comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:27:\"index.php/comments/embed/?$\";s:21:\"index.php?&embed=true\";s:54:\"index.php/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:49:\"index.php/search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:30:\"index.php/search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:42:\"index.php/search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:24:\"index.php/search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:57:\"index.php/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:45:\"index.php/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:27:\"index.php/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:79:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:55:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:49:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:66:\"index.php/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:42:\"index.php/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:54:\"index.php/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:36:\"index.php/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:53:\"index.php/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:48:\"index.php/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:29:\"index.php/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:41:\"index.php/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:23:\"index.php/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:68:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:78:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:98:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:74:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:63:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:87:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:75:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:71:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:57:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:67:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:87:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:63:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:48:\"index.php/([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:37:\"index.php/.?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"index.php/.?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"index.php/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"index.php/.?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"index.php/(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:30:\"index.php/(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:50:\"index.php/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:45:\"index.php/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:38:\"index.php/(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:45:\"index.php/(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:34:\"index.php/(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}','yes'),(30,'hack_file','0','yes'),(31,'blog_charset','UTF-8','yes'),(32,'moderation_keys','','no'),(33,'active_plugins','a:1:{i:0;s:34:\"social_media_plugin/fid_plugin.php\";}','yes'),(34,'category_base','','yes'),(35,'ping_sites','http://rpc.pingomatic.com/','yes'),(36,'comment_max_links','2','yes'),(37,'gmt_offset','0','yes'),(38,'default_email_category','1','yes'),(39,'recently_edited','','no'),(40,'template','FID-BD','yes'),(41,'stylesheet','FID-BD','yes'),(42,'comment_registration','0','yes'),(43,'html_type','text/html','yes'),(44,'use_trackback','0','yes'),(45,'default_role','subscriber','yes'),(46,'db_version','49752','yes'),(47,'uploads_use_yearmonth_folders','1','yes'),(48,'upload_path','','yes'),(49,'blog_public','1','yes'),(50,'default_link_category','2','yes'),(51,'show_on_front','posts','yes'),(52,'tag_base','','yes'),(53,'show_avatars','1','yes'),(54,'avatar_rating','G','yes'),(55,'upload_url_path','','yes'),(56,'thumbnail_size_w','150','yes'),(57,'thumbnail_size_h','150','yes'),(58,'thumbnail_crop','1','yes'),(59,'medium_size_w','300','yes'),(60,'medium_size_h','300','yes'),(61,'avatar_default','mystery','yes'),(62,'large_size_w','1024','yes'),(63,'large_size_h','1024','yes'),(64,'image_default_link_type','none','yes'),(65,'image_default_size','','yes'),(66,'image_default_align','','yes'),(67,'close_comments_for_old_posts','0','yes'),(68,'close_comments_days_old','14','yes'),(69,'thread_comments','1','yes'),(70,'thread_comments_depth','5','yes'),(71,'page_comments','0','yes'),(72,'comments_per_page','50','yes'),(73,'default_comments_page','newest','yes'),(74,'comment_order','asc','yes'),(75,'sticky_posts','a:0:{}','yes'),(76,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),(77,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),(78,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),(79,'uninstall_plugins','a:0:{}','no'),(80,'timezone_string','','yes'),(81,'page_for_posts','0','yes'),(82,'page_on_front','0','yes'),(83,'default_post_format','0','yes'),(84,'link_manager_enabled','0','yes'),(85,'finished_splitting_shared_terms','1','yes'),(86,'site_icon','0','yes'),(87,'medium_large_size_w','768','yes'),(88,'medium_large_size_h','0','yes'),(89,'wp_page_for_privacy_policy','3','yes'),(90,'show_comments_cookies_opt_in','1','yes'),(91,'admin_email_lifespan','1639487274','yes'),(92,'disallowed_keys','','no'),(93,'comment_previously_approved','1','yes'),(94,'auto_plugin_theme_update_emails','a:0:{}','no'),(95,'auto_update_core_dev','enabled','yes'),(96,'auto_update_core_minor','enabled','yes'),(97,'auto_update_core_major','enabled','yes'),(98,'initial_db_version','49752','yes'),(99,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),(100,'fresh_site','0','yes'),(101,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(102,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),(103,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),(104,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),(105,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(106,'sidebars_widgets','a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes'),(107,'cron','a:7:{i:1624262891;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1624280889;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1624280891;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1624281542;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1624281545;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1624626490;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}','yes'),(108,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(109,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(110,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(111,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(112,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(113,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(114,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(115,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(116,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(118,'recovery_keys','a:0:{}','yes'),(119,'theme_mods_twentytwentyone','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1623936055;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes'),(120,'https_detection_errors','a:0:{}','yes'),(121,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.7.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.7.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.7.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.7.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.2\";s:7:\"version\";s:5:\"5.7.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1624258141;s:15:\"version_checked\";s:5:\"5.7.2\";s:12:\"translations\";a:0:{}}','no'),(123,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1624258144;s:7:\"checked\";a:3:{s:19:\"akismet/akismet.php\";s:5:\"4.1.9\";s:9:\"hello.php\";s:5:\"1.7.2\";s:34:\"social_media_plugin/fid_plugin.php\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.9\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}','no'),(126,'_site_transient_update_themes','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1624258144;s:7:\"checked\";a:4:{s:6:\"FID-BD\";s:0:\"\";s:14:\"twentynineteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.7\";s:15:\"twentytwentyone\";s:3:\"1.3\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.0.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.7.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.3.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}','no'),(127,'_site_transient_timeout_browser_328ee23dea027c38139210175cf13ed7','1624540744','no'),(128,'_site_transient_browser_328ee23dea027c38139210175cf13ed7','a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"91.0.4472.101\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no'),(129,'_site_transient_timeout_php_check_8c0181da100b1c7d1f637c18117d0149','1624540745','no'),(130,'_site_transient_php_check_8c0181da100b1c7d1f637c18117d0149','a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no'),(134,'can_compress_scripts','0','no'),(149,'finished_updating_comment_type','1','yes'),(150,'current_theme','','yes'),(151,'theme_mods_FID-BD','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:8:\"menu-sup\";i:15;}s:18:\"custom_css_post_id\";i:-1;}','yes'),(152,'theme_switched','','yes'),(221,'category_children','a:0:{}','yes'),(239,'_transient_health-check-site-status-result','{\"good\":\"13\",\"recommended\":\"7\",\"critical\":\"0\"}','yes'),(242,'recently_activated','a:0:{}','yes'),(243,'_site_transient_timeout_browser_b36920373f87ae0a037579052d24624b','1624628765','no'),(244,'_site_transient_browser_b36920373f87ae0a037579052d24624b','a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"91.0.4472.101\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no'),(262,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes'),(269,'_site_transient_timeout_theme_roots','1624259943','no'),(270,'_site_transient_theme_roots','a:4:{s:6:\"FID-BD\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}','no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','default'),(2,3,'_wp_page_template','default'),(3,6,'_edit_lock','1623936658:1'),(4,5,'_edit_lock','1623936597:1'),(7,8,'_edit_lock','1623936548:1'),(8,9,'_edit_lock','1623936759:1'),(11,11,'_edit_lock','1623936692:1'),(12,13,'_edit_lock','1623936737:1'),(15,15,'_edit_lock','1623938337:1'),(16,16,'_edit_lock','1623938477:1'),(17,16,'date-event','25 septembre'),(18,16,'date-event','25 septembre'),(19,16,'_edit_last','1'),(20,16,'date-event','25 septembre'),(24,16,'_pingme','1'),(25,16,'_encloseme','1'),(26,19,'_edit_lock','1623938554:1'),(27,16,'_wp_trash_meta_status','publish'),(28,16,'_wp_trash_meta_status','publish'),(29,16,'_wp_trash_meta_time','1623938572'),(30,16,'_wp_trash_meta_time','1623938572'),(31,16,'_wp_desired_post_slug','test-date'),(32,16,'_wp_desired_post_slug','test-date'),(33,13,'_wp_trash_meta_status','publish'),(34,13,'_wp_trash_meta_status','publish'),(35,13,'_wp_trash_meta_time','1623938576'),(36,13,'_wp_trash_meta_time','1623938576'),(37,13,'_wp_desired_post_slug','avant-premiere'),(38,13,'_wp_desired_post_slug','avant-premiere'),(39,11,'_wp_trash_meta_status','draft'),(40,11,'_wp_trash_meta_time','1623938581'),(41,11,'_wp_desired_post_slug',''),(42,9,'_wp_trash_meta_status','publish'),(43,9,'_wp_trash_meta_time','1623938597'),(44,9,'_wp_desired_post_slug','concerts-soirees'),(45,1,'_wp_trash_meta_status','publish'),(46,1,'_wp_trash_meta_time','1623938608'),(47,1,'_wp_desired_post_slug','hello-world'),(48,1,'_wp_trash_meta_comments_status','a:1:{i:1;s:1:\"1\";}'),(49,6,'_wp_trash_meta_status','publish'),(50,6,'_wp_trash_meta_time','1623938612'),(51,6,'_wp_desired_post_slug','expositions-bd'),(52,21,'_edit_lock','1623940182:1'),(55,21,'_edit_last','1'),(56,21,'_pingme','1'),(57,21,'_encloseme','1'),(58,21,'_wp_trash_meta_status','publish'),(59,21,'_wp_trash_meta_time','1623940329'),(60,21,'_wp_desired_post_slug','21'),(61,23,'_edit_lock','1624000284:1'),(62,24,'_edit_lock','1624000208:1'),(63,25,'_edit_lock','1624000599:1'),(64,25,'Location of event',''),(65,25,'_edit_last','1'),(66,25,'Location of event',''),(67,27,'_edit_lock','1624000628:1'),(68,27,'Date event',''),(69,27,'_edit_last','1'),(70,27,'Date event',''),(71,29,'_edit_lock','1624000514:1'),(72,30,'_edit_lock','1624000544:1'),(73,31,'_edit_lock','1624000574:1'),(74,32,'_edit_lock','1624001713:1'),(75,25,'_wp_trash_meta_status','draft'),(76,25,'_wp_trash_meta_time','1624001503'),(77,25,'_wp_desired_post_slug',''),(78,27,'_wp_trash_meta_status','draft'),(79,27,'_wp_trash_meta_time','1624001507'),(80,27,'_wp_desired_post_slug',''),(81,32,'_wp_trash_meta_status','draft'),(82,32,'_wp_trash_meta_time','1624001510'),(83,32,'_wp_desired_post_slug',''),(84,34,'_edit_lock','1624001400:1'),(85,35,'_edit_lock','1624005766:1'),(86,36,'_edit_lock','1624002062:1'),(87,37,'_edit_lock','1624002141:1'),(90,37,'_edit_last','1'),(93,36,'_wp_trash_meta_status','draft'),(94,36,'_wp_trash_meta_time','1624002260'),(95,36,'_wp_desired_post_slug',''),(96,35,'_wp_trash_meta_status','draft'),(97,35,'_wp_trash_meta_time','1624002265'),(98,35,'_wp_desired_post_slug',''),(99,41,'_edit_lock','1624002571:1'),(100,41,'days event','Samedi e Dimanche'),(101,42,'_edit_lock','1624024538:1'),(103,42,'_edit_last','1'),(104,42,'days-event','Samedi et Dimanche'),(105,42,'date-event','26-27 Septembre 2020'),(106,42,'location-event','Église Dominicains, PERPIGNAN'),(111,45,'_edit_lock','1624003113:1'),(114,45,'_edit_last','1'),(117,47,'_edit_lock','1624026917:1'),(121,47,'_edit_last','1'),(124,47,'date-event','25 Septembre'),(127,50,'_edit_lock','1624027007:1'),(128,50,'date-event','25 Septembre  20 Décembre'),(131,50,'_edit_last','1'),(134,52,'_edit_lock','1624026941:1'),(135,52,'date-event','25 Septembre'),(138,52,'_edit_last','1'),(149,56,'_edit_lock','1624005456:1'),(150,57,'_wp_attached_file','2021/06/timbre-de-sponsor-86263619.jpg'),(151,57,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:626;s:4:\"file\";s:38:\"2021/06/timbre-de-sponsor-86263619.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"timbre-de-sponsor-86263619-300x235.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:235;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"timbre-de-sponsor-86263619-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"timbre-de-sponsor-86263619-768x601.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:601;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(152,58,'_wp_attached_file','2021/06/Carrefour-logo.png'),(153,58,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:940;s:6:\"height\";i:788;s:4:\"file\";s:26:\"2021/06/Carrefour-logo.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"Carrefour-logo-300x251.png\";s:5:\"width\";i:300;s:6:\"height\";i:251;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"Carrefour-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"Carrefour-logo-768x644.png\";s:5:\"width\";i:768;s:6:\"height\";i:644;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(154,59,'_wp_attached_file','2021/06/paradis.jpg'),(155,59,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:521;s:6:\"height\";i:367;s:4:\"file\";s:19:\"2021/06/paradis.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"paradis-300x211.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"paradis-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(156,60,'_edit_lock','1624019767:1'),(159,60,'_edit_last','1'),(162,62,'_edit_lock','1624019763:1'),(165,62,'_edit_last','1'),(168,64,'_edit_lock','1624024288:1'),(171,64,'_edit_last','1'),(180,64,'_thumbnail_id','59'),(187,60,'_thumbnail_id','58'),(192,62,'_thumbnail_id','57'),(199,41,'_wp_trash_meta_status','draft'),(200,41,'_wp_trash_meta_time','1624021810'),(201,41,'_wp_desired_post_slug',''),(213,75,'_menu_item_type','taxonomy'),(215,75,'_menu_item_menu_item_parent','0'),(217,75,'_menu_item_object_id','8'),(219,75,'_menu_item_object','category'),(221,75,'_menu_item_target',''),(223,75,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),(225,75,'_menu_item_xfn',''),(227,75,'_menu_item_url',''),(232,50,'_pingme','1'),(233,50,'_encloseme','1'),(234,76,'_wp_attached_file','2021/06/Mikailoff.jpg'),(235,76,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1307;s:4:\"file\";s:21:\"2021/06/Mikailoff.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"Mikailoff-235x300.jpg\";s:5:\"width\";i:235;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"Mikailoff-802x1024.jpg\";s:5:\"width\";i:802;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"Mikailoff-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"Mikailoff-768x980.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:980;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(236,77,'_wp_attached_file','2021/06/Mikailoff-1.jpg'),(237,77,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1307;s:4:\"file\";s:23:\"2021/06/Mikailoff-1.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Mikailoff-1-235x300.jpg\";s:5:\"width\";i:235;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"Mikailoff-1-802x1024.jpg\";s:5:\"width\";i:802;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Mikailoff-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"Mikailoff-1-768x980.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:980;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(238,78,'_wp_attached_file','2021/06/Mikailoff-2.jpg'),(239,78,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1307;s:4:\"file\";s:23:\"2021/06/Mikailoff-2.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Mikailoff-2-235x300.jpg\";s:5:\"width\";i:235;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"Mikailoff-2-802x1024.jpg\";s:5:\"width\";i:802;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Mikailoff-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"Mikailoff-2-768x980.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:980;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(240,79,'_wp_attached_file','2021/06/Mikailoff-3.jpg'),(241,79,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1307;s:4:\"file\";s:23:\"2021/06/Mikailoff-3.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Mikailoff-3-235x300.jpg\";s:5:\"width\";i:235;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"Mikailoff-3-802x1024.jpg\";s:5:\"width\";i:802;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Mikailoff-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"Mikailoff-3-768x980.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:980;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2021-06-17 13:08:06','2021-06-17 13:08:06','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Hello world!','','trash','open','open','','hello-world__trashed','','','2021-06-17 14:03:29','2021-06-17 14:03:29','',0,'http://tp-wordpress.lndo.site/?p=1',0,'post','',1),(2,1,'2021-06-17 13:08:06','2021-06-17 13:08:06','<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://tp-wordpress.lndo.site/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->','Sample Page','','publish','closed','open','','sample-page','','','2021-06-17 13:08:06','2021-06-17 13:08:06','',0,'http://tp-wordpress.lndo.site/?page_id=2',0,'page','',0),(3,1,'2021-06-17 13:08:06','2021-06-17 13:08:06','<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Our website address is: http://tp-wordpress.lndo.site.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comments</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Media</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Embedded content from other websites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph -->','Privacy Policy','','draft','closed','open','','privacy-policy','','','2021-06-17 13:08:06','2021-06-17 13:08:06','',0,'http://tp-wordpress.lndo.site/?page_id=3',0,'page','',0),(4,1,'2021-06-17 13:19:05','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-17 13:19:05','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=4',0,'post','',0),(5,1,'2021-06-17 13:29:56','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-17 13:29:56','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=5',0,'post','',0),(6,1,'2021-06-17 13:31:21','2021-06-17 13:31:21','<!-- wp:paragraph -->\n<p>Nous faisons des expos incroyables</p>\n<!-- /wp:paragraph -->','Expositions BD','','trash','open','open','','expositions-bd__trashed','','','2021-06-17 14:03:32','2021-06-17 14:03:32','',0,'http://tp-wordpress.lndo.site/?p=6',0,'post','',0),(7,1,'2021-06-17 13:31:21','2021-06-17 13:31:21','<!-- wp:paragraph -->\n<p>Nous faisons des expos incroyables</p>\n<!-- /wp:paragraph -->','Expositions BD','','inherit','closed','closed','','6-revision-v1','','','2021-06-17 13:31:21','2021-06-17 13:31:21','',6,'http://tp-wordpress.lndo.site/?p=7',0,'revision','',0),(8,1,'2021-06-17 13:31:30','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-17 13:31:30','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=8',0,'post','',0),(9,1,'2021-06-17 13:32:53','2021-06-17 13:32:53','<!-- wp:paragraph -->\n<p>Dans nos soirée et concerts il y a même des after... ;)</p>\n<!-- /wp:paragraph -->','Concerts Soirées','','trash','open','open','','concerts-soirees__trashed','','','2021-06-17 14:03:17','2021-06-17 14:03:17','',0,'http://tp-wordpress.lndo.site/?p=9',0,'post','',0),(10,1,'2021-06-17 13:32:53','2021-06-17 13:32:53','<!-- wp:paragraph -->\n<p>Dans nos soirée et concerts il y a même des after... ;)</p>\n<!-- /wp:paragraph -->','Concerts Soirées','','inherit','closed','closed','','9-revision-v1','','','2021-06-17 13:32:53','2021-06-17 13:32:53','',9,'http://tp-wordpress.lndo.site/?p=10',0,'revision','',0),(11,1,'2021-06-17 14:03:01','2021-06-17 14:03:01','<!-- wp:paragraph -->\n<p>Venez nous voir en avant première sur la planète mars </p>\n<!-- /wp:paragraph -->','Avant première','','trash','open','open','','__trashed','','','2021-06-17 14:03:01','2021-06-17 14:03:01','',0,'http://tp-wordpress.lndo.site/?p=11',0,'post','',0),(12,1,'2021-06-17 13:33:52','2021-06-17 13:33:52','<!-- wp:paragraph -->\n<p>Venez nous voir en avant première sur la planète mars </p>\n<!-- /wp:paragraph -->','Avant première','','inherit','closed','closed','','11-revision-v1','','','2021-06-17 13:33:52','2021-06-17 13:33:52','',11,'http://tp-wordpress.lndo.site/?p=12',0,'revision','',0),(13,1,'2021-06-17 13:34:38','2021-06-17 13:34:38','<!-- wp:paragraph -->\n<p>Venez nous voir en avant première sur la planète mars</p>\n<!-- /wp:paragraph -->','Avant première','','trash','open','open','','avant-premiere__trashed','','','2021-06-17 14:02:56','2021-06-17 14:02:56','',0,'http://tp-wordpress.lndo.site/?p=13',0,'post','',0),(14,1,'2021-06-17 13:34:38','2021-06-17 13:34:38','<!-- wp:paragraph -->\n<p>Venez nous voir en avant première sur la planète mars</p>\n<!-- /wp:paragraph -->','Avant première','','inherit','closed','closed','','13-revision-v1','','','2021-06-17 13:34:38','2021-06-17 13:34:38','',13,'http://tp-wordpress.lndo.site/?p=14',0,'revision','',0),(15,1,'2021-06-17 13:41:44','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-17 13:41:44','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=15',0,'post','',0),(16,1,'2021-06-17 14:01:14','2021-06-17 14:01:14','<!-- wp:paragraph -->\n<p>Hello world !</p>\n<!-- /wp:paragraph -->','test date','','trash','closed','closed','','test-date__trashed','','','2021-06-17 14:02:52','2021-06-17 14:02:52','',0,'http://tp-wordpress.lndo.site/?p=16',0,'post','',0),(17,1,'2021-06-17 14:00:48','2021-06-17 14:00:48','','Draft created on June 17, 2021 at 2:00 pm','','inherit','closed','closed','','16-revision-v1','','','2021-06-17 14:00:48','2021-06-17 14:00:48','',16,'http://tp-wordpress.lndo.site/?p=17',0,'revision','',0),(18,1,'2021-06-17 14:01:14','2021-06-17 14:01:14','<!-- wp:paragraph -->\n<p>Hello world !</p>\n<!-- /wp:paragraph -->','test date','','inherit','closed','closed','','16-revision-v1','','','2021-06-17 14:01:14','2021-06-17 14:01:14','',16,'http://tp-wordpress.lndo.site/?p=18',0,'revision','',0),(19,1,'2021-06-17 14:01:32','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-17 14:01:32','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=19',0,'post','',0),(20,1,'2021-06-17 14:03:29','2021-06-17 14:03:29','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Hello world!','','inherit','closed','closed','','1-revision-v1','','','2021-06-17 14:03:29','2021-06-17 14:03:29','',1,'http://tp-wordpress.lndo.site/?p=20',0,'revision','',0),(21,1,'2021-06-17 14:31:51','2021-06-17 14:31:51','<!-- wp:paragraph -->\n<p>mmmmmmmm</p>\n<!-- /wp:paragraph -->','','','trash','open','open','','21__trashed','','','2021-06-17 14:32:09','2021-06-17 14:32:09','',0,'http://tp-wordpress.lndo.site/?p=21',0,'post','',0),(22,1,'2021-06-17 14:31:51','2021-06-17 14:31:51','<!-- wp:paragraph -->\n<p>mmmmmmmm</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','21-revision-v1','','','2021-06-17 14:31:51','2021-06-17 14:31:51','',21,'http://tp-wordpress.lndo.site/?p=22',0,'revision','',0),(23,1,'2021-06-18 07:10:22','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 07:10:22','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=23',0,'post','',0),(24,1,'2021-06-18 07:11:36','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 07:11:36','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=24',0,'post','',0),(25,1,'2021-06-18 07:31:44','2021-06-18 07:31:44','','Draft created on June 18, 2021 at 7:16 am','','trash','closed','closed','','__trashed-2','','','2021-06-18 07:31:44','2021-06-18 07:31:44','',0,'http://tp-wordpress.lndo.site/?p=25',0,'post','',0),(26,1,'2021-06-18 07:16:38','2021-06-18 07:16:38','','Draft created on June 18, 2021 at 7:16 am','','inherit','closed','closed','','25-revision-v1','','','2021-06-18 07:16:38','2021-06-18 07:16:38','',25,'http://tp-wordpress.lndo.site/?p=26',0,'revision','',0),(27,1,'2021-06-18 07:31:47','2021-06-18 07:31:47','','Draft created on June 18, 2021 at 7:17 am','','trash','closed','closed','','__trashed-3','','','2021-06-18 07:31:47','2021-06-18 07:31:47','',0,'http://tp-wordpress.lndo.site/?p=27',0,'post','',0),(28,1,'2021-06-18 07:17:08','2021-06-18 07:17:08','','Draft created on June 18, 2021 at 7:17 am','','inherit','closed','closed','','27-revision-v1','','','2021-06-18 07:17:08','2021-06-18 07:17:08','',27,'http://tp-wordpress.lndo.site/?p=28',0,'revision','',0),(29,1,'2021-06-18 07:17:19','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 07:17:19','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=29',0,'post','',0),(30,1,'2021-06-18 07:17:40','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 07:17:40','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=30',0,'post','',0),(31,1,'2021-06-18 07:18:11','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 07:18:11','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=31',0,'post','',0),(32,1,'2021-06-18 07:31:50','2021-06-18 07:31:50','<!-- wp:paragraph -->\n<p>i</p>\n<!-- /wp:paragraph -->','','','trash','open','open','','__trashed-4','','','2021-06-18 07:31:50','2021-06-18 07:31:50','',0,'http://tp-wordpress.lndo.site/?p=32',0,'post','',0),(33,1,'2021-06-18 07:31:50','2021-06-18 07:31:50','<!-- wp:paragraph -->\n<p>i</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','32-revision-v1','','','2021-06-18 07:31:50','2021-06-18 07:31:50','',32,'http://tp-wordpress.lndo.site/?p=33',0,'revision','',0),(34,1,'2021-06-18 07:32:21','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 07:32:21','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=34',0,'post','',0),(35,1,'2021-06-18 07:44:25','2021-06-18 07:44:25','','F','','trash','open','open','','__trashed-6','','','2021-06-18 07:44:25','2021-06-18 07:44:25','',0,'http://tp-wordpress.lndo.site/?p=35',0,'post','',0),(36,1,'2021-06-18 07:44:20','2021-06-18 07:44:20','','d','','trash','open','open','','__trashed-5','','','2021-06-18 07:44:20','2021-06-18 07:44:20','',0,'http://tp-wordpress.lndo.site/?p=36',0,'post','',0),(37,1,'2021-06-18 07:44:00','2021-06-18 07:44:00','<!-- wp:paragraph -->\n<p>Demain, dans mon salon</p>\n<!-- /wp:paragraph -->','','','publish','open','open','','37','','','2021-06-18 07:44:02','2021-06-18 07:44:02','',0,'http://tp-wordpress.lndo.site/?p=37',0,'post','',0),(38,1,'2021-06-18 07:44:00','2021-06-18 07:44:00','<!-- wp:paragraph -->\n<p>Demain, dans mon salon</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','37-revision-v1','','','2021-06-18 07:44:00','2021-06-18 07:44:00','',37,'http://tp-wordpress.lndo.site/?p=38',0,'revision','',0),(39,1,'2021-06-18 07:44:20','2021-06-18 07:44:20','','d','','inherit','closed','closed','','36-revision-v1','','','2021-06-18 07:44:20','2021-06-18 07:44:20','',36,'http://tp-wordpress.lndo.site/?p=39',0,'revision','',0),(40,1,'2021-06-18 07:44:25','2021-06-18 07:44:25','','F','','inherit','closed','closed','','35-revision-v1','','','2021-06-18 07:44:25','2021-06-18 07:44:25','',35,'http://tp-wordpress.lndo.site/?p=40',0,'revision','',0),(41,1,'2021-06-18 13:10:10','2021-06-18 13:10:10','<!-- wp:paragraph -->\n<p>“Le plus grand magasin de disques et BD du monde”, le temps du dernier week-end de Septembre avec 80 stands d’exposants nationaux et internationaux, dans un lieu unique et hors du commun : L\'église des Dominicains de Perpignan. Plus grande foire Euro Méditerranéenne du disque avec plus de 4000 visiteurs, c’est LE Festival POP ROCK ! C\'est aussi un espace convivial de rencontres, d\'échanges autour des métiers du disque, de la musique, de la BD, du livre, de la photographie et de la contre-culture.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> </p>\n<!-- /wp:paragraph -->','','','trash','open','open','','__trashed-7','','','2021-06-18 13:10:10','2021-06-18 13:10:10','',0,'http://tp-wordpress.lndo.site/?p=41',0,'post','',0),(42,1,'2021-06-18 07:55:17','2021-06-18 07:55:17','<!-- wp:paragraph -->\n<p>“Le plus grand magasin de disques et BD du monde”, le temps du dernier week-end de Septembre avec 80 stands d’exposants nationaux et internationaux, dans un lieu unique et hors du commun : L\'église des Dominicains de Perpignan. Plus grande foire Euro-Méditerranéenne du disque avec plus de 4000 visiteurs, c’est LE Festival POP ROCK ! C\'est aussi un espace convivial de rencontres, d\'échanges autour des métiers du disque, de la musique, de la BD, du livre, de la photographie et de la contre-culture.</p>\n<!-- /wp:paragraph -->','','','publish','closed','closed','','42','','','2021-06-18 13:54:45','2021-06-18 13:54:45','',0,'http://tp-wordpress.lndo.site/?p=42',0,'post','',0),(43,1,'2021-06-18 07:51:24','2021-06-18 07:51:24','','Draft created on June 18, 2021 at 7:51 am','','inherit','closed','closed','','42-revision-v1','','','2021-06-18 07:51:24','2021-06-18 07:51:24','',42,'http://tp-wordpress.lndo.site/?p=43',0,'revision','',0),(44,1,'2021-06-18 07:55:17','2021-06-18 07:55:17','<!-- wp:paragraph -->\n<p>“Le&nbsp;plus&nbsp;grand&nbsp;magasin&nbsp;de&nbsp;disques&nbsp;et&nbsp;BD&nbsp;du&nbsp;monde”,&nbsp;le&nbsp;temps&nbsp;du&nbsp;dernier&nbsp;week-end&nbsp;de&nbsp;Septembre&nbsp;avec&nbsp;80&nbsp;stands&nbsp;d’exposants&nbsp;nationaux&nbsp;et&nbsp;internationaux,&nbsp;dans&nbsp;un&nbsp;lieu&nbsp;unique&nbsp;et&nbsp;hors&nbsp;du&nbsp;commun&nbsp;:&nbsp;L\'église&nbsp;des&nbsp;Dominicains&nbsp;de&nbsp;Perpignan.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plus&nbsp;grande&nbsp;foire&nbsp;Euro-Méditerranéenne&nbsp;du&nbsp;disque&nbsp;avec&nbsp;plus&nbsp;de&nbsp;4000&nbsp;visiteurs,&nbsp;c’est&nbsp;LE&nbsp;Festival&nbsp;POP&nbsp;ROCK&nbsp;!&nbsp;C\'est&nbsp;aussi&nbsp;un&nbsp;espace&nbsp;convivial&nbsp;de&nbsp;rencontres,&nbsp;d\'échanges&nbsp;autour&nbsp;des&nbsp;métiers&nbsp;du&nbsp;disque,&nbsp;de&nbsp;la&nbsp;musique,&nbsp;de&nbsp;la&nbsp;BD,&nbsp;du&nbsp;livre,&nbsp;de&nbsp;la&nbsp;photographie&nbsp;et&nbsp;de&nbsp;la&nbsp;contre-culture.</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','42-revision-v1','','','2021-06-18 07:55:17','2021-06-18 07:55:17','',42,'http://tp-wordpress.lndo.site/?p=44',0,'revision','',0),(45,1,'2021-06-18 07:58:29','2021-06-18 07:58:29','<!-- wp:paragraph -->\n<p>Gratuité des expositions et des concerts / Entrée du FID : 3€</p>\n<!-- /wp:paragraph -->','','','publish','open','open','','45','','','2021-06-18 07:58:32','2021-06-18 07:58:32','',0,'http://tp-wordpress.lndo.site/?p=45',0,'post','',0),(46,1,'2021-06-18 07:58:29','2021-06-18 07:58:29','<!-- wp:paragraph -->\n<p>Gratuité des expositions et des concerts / Entrée du FID : 3€</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','45-revision-v1','','','2021-06-18 07:58:29','2021-06-18 07:58:29','',45,'http://tp-wordpress.lndo.site/?p=46',0,'revision','',0),(47,1,'2021-06-18 08:25:14','2021-06-18 08:25:14','<!-- wp:paragraph -->\n<p>Frank&nbsp;Margerin,&nbsp;Alexandre&nbsp;Clérisse,&nbsp;Steve&nbsp;Galliot-Villers,&nbsp;et&nbsp;Elric&nbsp;Dufau&nbsp;seront&nbsp;là&nbsp;pour&nbsp;dédicacer&nbsp;leurs&nbsp;derniers&nbsp;ouvrages.À&nbsp;partir&nbsp;de&nbsp;16h.</p>\n<!-- /wp:paragraph -->','Vente dédicace  Bédé en bulles (10 rue de la Cloche d’Or)','','publish','open','open','','vente-dedicace-bede-en-bulles-10-rue-de-la-cloche-dor','','','2021-06-18 08:34:08','2021-06-18 08:34:08','',0,'http://tp-wordpress.lndo.site/?p=47',0,'post','',0),(48,1,'2021-06-18 08:25:14','2021-06-18 08:25:14','<!-- wp:paragraph -->\n<p>Frank Margerin, Alexandre Clérisse, Steve Galliot-Villers, et Elric Dufau seront là pour dédicacer leurs derniers ouvrages.&lt;br>À partir de 16h.</p>\n<!-- /wp:paragraph -->','Vente dédicace  Bédé en bulles (10 rue de la Cloche d’Or)','','inherit','closed','closed','','47-revision-v1','','','2021-06-18 08:25:14','2021-06-18 08:25:14','',47,'http://tp-wordpress.lndo.site/?p=48',0,'revision','',0),(49,1,'2021-06-18 08:25:23','2021-06-18 08:25:23','<!-- wp:paragraph -->\n<p>Frank Margerin, Alexandre Clérisse, Steve Galliot-Villers, et Elric Dufau seront là pour dédicacer leurs derniers ouvrages.À partir de 16h.</p>\n<!-- /wp:paragraph -->','Vente dédicace  Bédé en bulles (10 rue de la Cloche d’Or)','','inherit','closed','closed','','47-revision-v1','','','2021-06-18 08:25:23','2021-06-18 08:25:23','',47,'http://tp-wordpress.lndo.site/?p=49',0,'revision','',0),(50,1,'2021-06-18 08:29:27','2021-06-18 08:29:27','<!-- wp:paragraph -->\n<p>Exposition&nbsp;de&nbsp;dessins&nbsp;\"Jampur&nbsp;Fraize&nbsp;et&nbsp;Mezzo&nbsp;à&nbsp;Elmediator\". Ouvert&nbsp;au&nbsp;public&nbsp;tous&nbsp;les&nbsp;soirs&nbsp;de&nbsp;concert.</p>\n<!-- /wp:paragraph -->','Exposition à Elmediator (avenue du Général Leclerc)','','publish','open','open','','exposition-a-elmediator-avenue-du-general-leclerc','','','2021-06-18 14:30:56','2021-06-18 14:30:56','',0,'http://tp-wordpress.lndo.site/?p=50',0,'post','',0),(51,1,'2021-06-18 08:29:27','2021-06-18 08:29:27','<!-- wp:paragraph -->\n<p>Exposition de dessins \"Jampur Fraize et Mezzo à Elmediator\". Ouvert au public tous les soirs de concert.</p>\n<!-- /wp:paragraph -->','Exposition à Elmediator (avenue du Général Leclerc)','','inherit','closed','closed','','50-revision-v1','','','2021-06-18 08:29:27','2021-06-18 08:29:27','',50,'http://tp-wordpress.lndo.site/?p=51',0,'revision','',0),(52,1,'2021-06-18 08:33:37','2021-06-18 08:33:37','<!-- wp:paragraph -->\n<p>DJ Battle avec Rufsig, M. Perez, Riff Reb’s, J. Casagran, L. Limiñana et Raph Dumas.  À partir de 21h. Entrée gratuite.</p>\n<!-- /wp:paragraph -->','DJ Battle au TÊT (76, avenue Louis Torcatis)','','publish','open','open','','dj-battle-au-tet-76-avenue-louis-torcatis','','','2021-06-18 08:33:40','2021-06-18 08:33:40','',0,'http://tp-wordpress.lndo.site/?p=52',0,'post','',0),(53,1,'2021-06-18 08:33:37','2021-06-18 08:33:37','<!-- wp:paragraph -->\n<p>DJ Battle avec Rufsig, M. Perez, Riff Reb’s, J. Casagran, L. Limiñana et Raph Dumas.  À partir de 21h. Entrée gratuite.</p>\n<!-- /wp:paragraph -->','DJ Battle au TÊT (76, avenue Louis Torcatis)','','inherit','closed','closed','','52-revision-v1','','','2021-06-18 08:33:37','2021-06-18 08:33:37','',52,'http://tp-wordpress.lndo.site/?p=53',0,'revision','',0),(54,1,'2021-06-18 08:34:06','2021-06-18 08:34:06','<!-- wp:paragraph -->\n<p>Frank&nbsp;Margerin,&nbsp;Alexandre&nbsp;Clérisse,&nbsp;Steve&nbsp;Galliot-Villers,&nbsp;et&nbsp;Elric&nbsp;Dufau&nbsp;seront&nbsp;là&nbsp;pour&nbsp;dédicacer&nbsp;leurs&nbsp;derniers&nbsp;ouvrages.À&nbsp;partir&nbsp;de&nbsp;16h.</p>\n<!-- /wp:paragraph -->','Vente dédicace  Bédé en bulles (10 rue de la Cloche d’Or)','','inherit','closed','closed','','47-revision-v1','','','2021-06-18 08:34:06','2021-06-18 08:34:06','',47,'http://tp-wordpress.lndo.site/?p=54',0,'revision','',0),(55,1,'2021-06-18 08:34:31','2021-06-18 08:34:31','<!-- wp:paragraph -->\n<p>Exposition&nbsp;de&nbsp;dessins&nbsp;\"Jampur&nbsp;Fraize&nbsp;et&nbsp;Mezzo&nbsp;à&nbsp;Elmediator\". Ouvert&nbsp;au&nbsp;public&nbsp;tous&nbsp;les&nbsp;soirs&nbsp;de&nbsp;concert.</p>\n<!-- /wp:paragraph -->','Exposition à Elmediator (avenue du Général Leclerc)','','inherit','closed','closed','','50-revision-v1','','','2021-06-18 08:34:31','2021-06-18 08:34:31','',50,'http://tp-wordpress.lndo.site/?p=55',0,'revision','',0),(56,1,'2021-06-18 08:34:35','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2021-06-18 08:34:35','0000-00-00 00:00:00','',0,'http://tp-wordpress.lndo.site/?p=56',0,'post','',0),(57,1,'2021-06-18 08:38:11','2021-06-18 08:38:11','','timbre-de-sponsor-86263619','','inherit','open','closed','','timbre-de-sponsor-86263619','','','2021-06-18 08:38:11','2021-06-18 08:38:11','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/timbre-de-sponsor-86263619.jpg',0,'attachment','image/jpeg',0),(58,1,'2021-06-18 08:38:27','2021-06-18 08:38:27','','Carrefour-logo','','inherit','open','closed','','carrefour-logo','','','2021-06-18 08:38:27','2021-06-18 08:38:27','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/Carrefour-logo.png',0,'attachment','image/png',0),(59,1,'2021-06-18 08:38:49','2021-06-18 08:38:49','','paradis','','inherit','open','closed','','paradis','','','2021-06-18 08:38:49','2021-06-18 08:38:49','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/paradis.jpg',0,'attachment','image/jpeg',0),(60,1,'2021-06-18 08:39:58','2021-06-18 08:39:58','<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','$$$$','','publish','open','open','','60','','','2021-06-18 12:37:46','2021-06-18 12:37:46','',0,'http://tp-wordpress.lndo.site/?p=60',0,'post','',0),(61,1,'2021-06-18 08:39:58','2021-06-18 08:39:58','<!-- wp:image {\"id\":59,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/paradis.jpg\" alt=\"\" class=\"wp-image-59\"/></figure>\n<!-- /wp:image -->','','','inherit','closed','closed','','60-revision-v1','','','2021-06-18 08:39:58','2021-06-18 08:39:58','',60,'http://tp-wordpress.lndo.site/?p=61',0,'revision','',0),(62,1,'2021-06-18 08:40:35','2021-06-18 08:40:35','<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','','','publish','open','open','','62','','','2021-06-18 12:38:22','2021-06-18 12:38:22','',0,'http://tp-wordpress.lndo.site/?p=62',0,'post','',0),(63,1,'2021-06-18 08:40:35','2021-06-18 08:40:35','<!-- wp:image {\"id\":58,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/Carrefour-logo.png\" alt=\"\" class=\"wp-image-58\"/></figure>\n<!-- /wp:image -->','','','inherit','closed','closed','','62-revision-v1','','','2021-06-18 08:40:35','2021-06-18 08:40:35','',62,'http://tp-wordpress.lndo.site/?p=63',0,'revision','',0),(64,1,'2021-06-18 08:41:01','2021-06-18 08:41:01','','wordpress','','publish','open','open','','64','','','2021-06-18 13:51:27','2021-06-18 13:51:27','',0,'http://tp-wordpress.lndo.site/?p=64',0,'post','',0),(65,1,'2021-06-18 08:41:01','2021-06-18 08:41:01','<!-- wp:image {\"id\":57,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/timbre-de-sponsor-86263619.jpg\" alt=\"\" class=\"wp-image-57\"/></figure>\n<!-- /wp:image -->','','','inherit','closed','closed','','64-revision-v1','','','2021-06-18 08:41:01','2021-06-18 08:41:01','',64,'http://tp-wordpress.lndo.site/?p=65',0,'revision','',0),(67,1,'2021-06-18 12:37:15','2021-06-18 12:37:15','','wordpress','','inherit','closed','closed','','64-revision-v1','','','2021-06-18 12:37:15','2021-06-18 12:37:15','',64,'http://tp-wordpress.lndo.site/?p=67',0,'revision','',0),(68,1,'2021-06-18 12:37:44','2021-06-18 12:37:44','<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','$$$$','','inherit','closed','closed','','60-revision-v1','','','2021-06-18 12:37:44','2021-06-18 12:37:44','',60,'http://tp-wordpress.lndo.site/?p=68',0,'revision','',0),(69,1,'2021-06-18 12:38:20','2021-06-18 12:38:20','<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','62-revision-v1','','','2021-06-18 12:38:20','2021-06-18 12:38:20','',62,'http://tp-wordpress.lndo.site/?p=69',0,'revision','',0),(70,1,'2021-06-18 12:45:48','2021-06-18 12:45:48','<!-- wp:paragraph -->\n<p>“Le plus grand magasin de disques et BD du monde”, le temps du dernier week-end de Septembre avec 80 stands d’exposants nationaux et internationaux, dans un lieu unique et hors du commun : L\'église des Dominicains de Perpignan. Plus grande foire Euro-Méditerranéenne du disque avec plus de 4000 visiteurs, c’est LE Festival POP ROCK ! C\'est aussi un espace convivial de rencontres, d\'échanges autour des métiers du disque, de la musique, de la BD, du livre, de la photographie et de la contre-culture.</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','42-revision-v1','','','2021-06-18 12:45:48','2021-06-18 12:45:48','',42,'http://tp-wordpress.lndo.site/?p=70',0,'revision','',0),(71,1,'2021-06-18 13:10:10','2021-06-18 13:10:10','<!-- wp:paragraph -->\n<p>“Le plus grand magasin de disques et BD du monde”, le temps du dernier week-end de Septembre avec 80 stands d’exposants nationaux et internationaux, dans un lieu unique et hors du commun : L\'église des Dominicains de Perpignan. Plus grande foire Euro Méditerranéenne du disque avec plus de 4000 visiteurs, c’est LE Festival POP ROCK ! C\'est aussi un espace convivial de rencontres, d\'échanges autour des métiers du disque, de la musique, de la BD, du livre, de la photographie et de la contre-culture.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> </p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','41-revision-v1','','','2021-06-18 13:10:10','2021-06-18 13:10:10','',41,'http://tp-wordpress.lndo.site/?p=71',0,'revision','',0),(72,1,'2021-06-18 13:53:48','2021-06-18 13:53:48','<!-- wp:paragraph -->\n<p>“</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>bobLe plus grand magasin de disques et BD du monde”, le temps du dernier week-end de Septembre avec 80 stands d’exposants nationaux et internationaux, dans un lieu unique et hors du commun : L\'église des Dominicains de Perpignan. Plus grande foire Euro-Méditerranéenne du disque avec plus de 4000 visiteurs, c’est LE Festival POP ROCK ! C\'est aussi un espace convivial de rencontres, d\'échanges autour des métiers du disque, de la musique, de la BD, du livre, de la photographie et de la contre-culture.</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','42-revision-v1','','','2021-06-18 13:53:48','2021-06-18 13:53:48','',42,'http://tp-wordpress.lndo.site/?p=72',0,'revision','',0),(73,1,'2021-06-18 13:54:44','2021-06-18 13:54:44','<!-- wp:paragraph -->\n<p>“Le plus grand magasin de disques et BD du monde”, le temps du dernier week-end de Septembre avec 80 stands d’exposants nationaux et internationaux, dans un lieu unique et hors du commun : L\'église des Dominicains de Perpignan. Plus grande foire Euro-Méditerranéenne du disque avec plus de 4000 visiteurs, c’est LE Festival POP ROCK ! C\'est aussi un espace convivial de rencontres, d\'échanges autour des métiers du disque, de la musique, de la BD, du livre, de la photographie et de la contre-culture.</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','42-revision-v1','','','2021-06-18 13:54:44','2021-06-18 13:54:44','',42,'http://tp-wordpress.lndo.site/?p=73',0,'revision','',0),(75,1,'2021-06-18 14:09:00','2021-06-18 13:58:56',' ','','','publish','closed','closed','','75','','','2021-06-18 14:09:00','2021-06-18 14:09:00','',0,'http://tp-wordpress.lndo.site/?p=75',1,'nav_menu_item','',0),(76,1,'2021-06-21 06:58:10','2021-06-21 06:58:10','','Mikailoff','','inherit','open','closed','','mikailoff','','','2021-06-21 06:58:10','2021-06-21 06:58:10','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/Mikailoff.jpg',0,'attachment','image/jpeg',0),(77,1,'2021-06-21 07:00:35','2021-06-21 07:00:35','','Mikailoff-1','','inherit','open','closed','','mikailoff-1','','','2021-06-21 07:00:35','2021-06-21 07:00:35','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/Mikailoff-1.jpg',0,'attachment','image/jpeg',0),(78,1,'2021-06-21 07:03:32','2021-06-21 07:03:32','','Mikailoff-2','','inherit','open','closed','','mikailoff-2','','','2021-06-21 07:03:32','2021-06-21 07:03:32','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/Mikailoff-2.jpg',0,'attachment','image/jpeg',0),(79,1,'2021-06-21 07:05:27','2021-06-21 07:05:27','','Mikailoff-3','','inherit','open','closed','','mikailoff-3','','','2021-06-21 07:05:27','2021-06-21 07:05:27','',0,'http://tp-wordpress.lndo.site/wp-content/uploads/2021/06/Mikailoff-3.jpg',0,'attachment','image/jpeg',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,1,0),(6,6,0),(9,7,0),(11,1,0),(13,5,0),(16,1,0),(21,1,0),(25,1,0),(27,1,0),(32,1,0),(35,1,0),(36,1,0),(37,12,0),(41,1,0),(42,8,0),(45,14,0),(47,5,0),(50,6,0),(52,7,0),(60,11,0),(62,11,0),(64,11,0),(75,15,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,0),(5,5,'category','',0,1),(6,6,'category','',0,1),(7,7,'category','',0,1),(8,8,'category','',0,1),(11,11,'category','',0,3),(12,12,'category','',0,1),(14,14,'category','',0,1),(15,15,'nav_menu','',0,1);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0),(5,'Avant-première','avant-premiere',0),(6,'Expositions-BD','expositions-bd',0),(7,'Concerts-soirées','concerts-soirees',0),(8,'A-propos du festival','a-propos-du-festival',0),(11,'Mes partenaires','mes-partenaires',0),(12,'Date et lieu du festival','date-et-lieu-du-festival',0),(14,'Tarif du festival','tarif',0),(15,'fdsq','fdsq',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_urls`
--

DROP TABLE IF EXISTS `wp_urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_urls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guests_id` int(10) unsigned DEFAULT NULL,
  `network_id` int(10) unsigned DEFAULT NULL,
  `url_network` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_urls`
--

LOCK TABLES `wp_urls` WRITE;
/*!40000 ALTER TABLE `wp_urls` DISABLE KEYS */;
INSERT INTO `wp_urls` VALUES (1,1,1,''),(2,1,2,'https://twitter.com/pierremikailoff'),(3,1,3,''),(4,1,4,''),(5,1,5,''),(6,1,6,'');
/*!40000 ALTER TABLE `wp_urls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','admin'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'syntax_highlighting','true'),(7,1,'comment_shortcuts','false'),(8,1,'admin_color','fresh'),(9,1,'use_ssl','0'),(10,1,'show_admin_bar_front','true'),(11,1,'locale',''),(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(13,1,'wp_user_level','10'),(14,1,'dismissed_wp_pointers',''),(15,1,'show_welcome_panel','1'),(16,1,'session_tokens','a:2:{s:64:\"ac0ee19b867e1f7c4d3c4dee4f448d8e70d9cbcefaf1fdf089ead9cdba79ccdb\";a:4:{s:10:\"expiration\";i:1625145541;s:2:\"ip\";s:10:\"172.20.0.2\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36\";s:5:\"login\";i:1623935941;}s:64:\"2133bc7a807ed81ccd71577dd176063c56ba585ad91af2e5a200f0989fda5430\";a:4:{s:10:\"expiration\";i:1624430970;s:2:\"ip\";s:10:\"172.20.0.2\";s:2:\"ua\";s:131:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36 Edg/91.0.864.48\";s:5:\"login\";i:1624258170;}}'),(17,1,'wp_dashboard_quick_press_last_post_id','4'),(18,1,'community-events-location','a:1:{s:2:\"ip\";s:10:\"172.20.0.0\";}'),(19,1,'enable_custom_fields','1'),(20,1,'meta-box-order_','a:3:{s:6:\"normal\";s:10:\"postcustom\";s:4:\"side\";s:0:\"\";s:8:\"advanced\";s:0:\"\";}'),(21,1,'closedpostboxes_','a:0:{}'),(22,1,'metaboxhidden_','a:0:{}'),(23,1,'wp_user-settings','libraryContent=browse'),(24,1,'wp_user-settings-time','1624005596'),(25,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),(26,1,'metaboxhidden_nav-menus','a:1:{i:0;s:12:\"add-post_tag\";}');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'admin','$P$BU4ofc.YhsQKoCUlPE/XioEgdaeDos0','admin','no-reply@extly.com','http://tp-wordpress.lndo.site','2021-06-17 13:08:04','',0,'admin');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-21  9:26:19
